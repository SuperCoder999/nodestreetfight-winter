const UserService = require('../services/userService');

class UserController {
    list(req, res, next) {
        res.data = { json: UserService.list() };
        next();
    }

    retreive(req, res, next) {
        const result = UserService.retreive({ id: req.params.id });
        res.data = { json: result };
        next();
    }

    create(req, res, next) {
        const result = UserService.create(req.body);
        res.data = { json: result };
        next();
    }

    partialUpdate(req, res, next) {
        const result = UserService.partialUpdate(req.params.id, req.body);
        res.data = { json: result };
        next();
    }

    delete(req, res, next) {
        const result = UserService.delete(req.params.id);
        res.data = { json: result };
        next();
    }
}

module.exports = new UserController();
