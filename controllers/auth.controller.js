const AuthService = require('../services/authService');

class AuthController {
    login(req, res, next) {
        const user = AuthService.login(req.body);
        res.data = { json: user };
        next();
    }
}

module.exports = new AuthController();
