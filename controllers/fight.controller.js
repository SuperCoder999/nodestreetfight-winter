const FightService = require('../services/fightService');

class FightController {
    list(req, res, next) {
        res.data = { json: FightService.list() };
        next();
    }

    retreive(req, res, next) {
        const result = FightService.retreive({ id: req.params.id });
        res.data = { json: result };
        next();
    }

    create(req, res, next) {
        const result = FightService.create(req.body);
        res.data = { json: result };
        next();
    }

    partialUpdate(req, res, next) {
        const result = FightService.partialUpdate(req.params.id, req.body);
        res.data = { json: result };
        next();
    }

    delete(req, res, next) {
        const result = FightService.delete(req.params.id);
        res.data = { json: result };
        next();
    }
}

module.exports = new FightController();
