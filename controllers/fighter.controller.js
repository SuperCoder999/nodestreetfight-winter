const FighterService = require('../services/fighterService');

class FighterController {
    list(req, res, next) {
        const result = FighterService.list();
        res.data = { json: result };
        next();
    }

    retreive(req, res, next) {
        const result = FighterService.retreive({ id: req.params.id });
        res.data = { json: result };
        next();
    }

    create(req, res, next) {
        const result = FighterService.create(req.body);
        res.data = { json: result };
        next();
    }

    partialUpdate(req, res, next) {
        const result = FighterService.partialUpdate(req.params.id, req.body);
        res.data = { json: result };
        next();
    }

    delete(req, res, next) {
        const result = FighterService.delete(req.params.id);
        res.data = { json: result };
        next();
    }
}

module.exports = new FighterController();