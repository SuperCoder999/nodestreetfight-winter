const { ErrorMessages } = require('../config/error');

const validationTemplate = {
    email: '',
    password: '',
};

const validateMissing = (data, tmp) => {
    for (const key in tmp) {
        if (!data.hasOwnProperty(key)) {
            throw {
                message: ErrorMessages.CredentialsInvalid,
            };
        }
    }
}

const validateExcess = (data, tmp) => {
    for (const key in data) {
        if (!tmp.hasOwnProperty(key)) {
            throw {
                message: key + ErrorMessages.Excess,
            };
        }
    }
};

const loginValid = (req, res, next) => {
    const data = req.body;

    validateMissing(data, validationTemplate);
    validateExcess(data, validationTemplate);
    next();
};

exports.loginValid = loginValid;