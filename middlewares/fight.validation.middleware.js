const { validationTemplate } = require('../models/fight');
const { ErrorMessages } = require('../config/error');

const validateMissing = (data, tmp) => {
    for (const key in tmp) {
        if (!data.hasOwnProperty(key)) {
            throw {
                message: key + ErrorMessages.Missing
            };
        }
    }
};

const validateExcess = (data, tmp) => {
    for (const key in data) {
        if (!tmp.hasOwnProperty(key)) {
            throw {
                message: key + ErrorMessages.Excess,
            };
        }
    }
}

const createFightValid = (req, res, next) => {
    req.body.log = req.body.log || [];
    const data = req.body;

    validateMissing(data, validationTemplate);
    validateExcess(data, validationTemplate);
    next();
};

const updateFightValid = (req, res, next) => {
    const data = req.body;

    validateExcess(data, validationTemplate);
    next();
};

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;
