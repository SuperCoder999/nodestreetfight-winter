const { ErrorMessages } = require('../config/error');
const { ValidationRegexes, ValidationNumbers } = require('../config/validation');
const { validationTemplate } = require('../models/user');
const { UserRepository } = require('../repositories/userRepository');

const emailIsValid = (email) => {
    if (email) {
        const emailValid = ValidationRegexes.Email.test(email);

        if (!emailValid) {
            throw {
                message: ErrorMessages.InvalidEmail,
            };
        }
    }
};

const phoneNumberIsValid = (phoneNumber) => {
    if (phoneNumber) {
        const phoneValid = ValidationRegexes.Phone.test(phoneNumber);

        if (!phoneValid) {
            throw {
                message: ErrorMessages.InvalidPhone,
            };
        }
    }
};

const passwordIsValid = (password) => {
    if (password) {
        const passwordValid = password.length >= ValidationNumbers.PasswordMin;

        if (!passwordValid) {
            throw {
                message: ErrorMessages.ShortPassword,
            };
        }
    }
};

const validateUnique = (data, id) => {
    const email = data.email;
    const phone = data.phoneNumber;

    if (email || phone) {
        const users = UserRepository.getAll();
        
        if (email) {
            const emailCheck = users.find((user) => (user.id !== id) && (user.email === email));

            if (emailCheck) {
                throw {
                    message: ErrorMessages.EmailNonUnique
                };
            }
        }
        
        if (phone) {
            const phoneCheck = users.find((user) => (user.id !== id) && (user.phoneNumber === phone));

            if (phoneCheck) {
                throw {
                    message: ErrorMessages.PhoneNonUnique
                };
            }
        }
    }
}

const basicUserValid = (data, id) => {
    validateExcess(data, validationTemplate);
    emailIsValid(data.email);
    phoneNumberIsValid(data.phoneNumber);
    passwordIsValid(data.password);
    validateUnique(data, id);
};

const validateMissing = (data, tmp) => {
    for (const key in tmp) {
        if (!data.hasOwnProperty(key)) {
            throw {
                message: key + ErrorMessages.Missing,
            };
        }
    }
};

const validateExcess = (data, tmp) => {
    for (const key in data) {
        if (!tmp.hasOwnProperty(key)) {
            throw {
                message: key + ErrorMessages.Excess,
            };
        }
    }
};

const createUserValid = (req, res, next) => {
    const data = req.body;

    validateMissing(data, validationTemplate);
    basicUserValid(data, null);
    next();
};

const updateUserValid = (req, res, next) => {
    const data = req.body;

    basicUserValid(data, req.params.id);
    next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;