const { validationTemplate } = require('../models/fighter');
const { ErrorMessages } = require('../config/error');
const { ValidationNumbers } = require('../config/validation');
const { FighterRepository } = require('../repositories/fighterRepository');

const powerIsValid = (power) => {
    if (power) {
        const powerValid = typeof power === 'number'
            && power <= ValidationNumbers.PowerMax && power >= ValidationNumbers.PowerMin;

        if (!powerValid) {
            throw {
                message: ErrorMessages.InvalidPower,
            };
        }
    }
}

const defenseIsValid = (defense) => {
    if (defense) {
        const defenseValid = typeof defense === 'number'
            && defense <= ValidationNumbers.DefenseMax && defense >= ValidationNumbers.DefenseMin;

        if (!defenseValid) {
            throw {
                message: ErrorMessages.InvalidPower,
            };
        }
    }
}

const validateUniqueName = (name, id) => {
    if (name) {
        const fighters = FighterRepository.getAll();
        const check = fighters.find((fighter) => (fighter.id !== id) && (fighter.name === name));

        if (check) {
            throw {
                message: ErrorMessages.NameNonUnique,
            };
        }
    }
}

const basicFighterValid = (data, id) => {
    validateUniqueName(data.name, id);
    validateExcess(data, validationTemplate);
    powerIsValid(data.power);
    defenseIsValid(data.defense);
}

const validateMissing = (data, tmp) => {
    for (const key in tmp) {
        if (!data.hasOwnProperty(key)) {
            throw {
                message: key + ErrorMessages.Missing,
            };
        }
    }
}

const validateExcess = (data, tmp) => {
    for (const key in data) {
        if (!tmp.hasOwnProperty(key)) {
            throw {
                message: key + ErrorMessages.Excess,
            };
        }
    }
}

const createFighterValid = (req, res, next) => {
    const data = req.body;

    validateMissing(data, validationTemplate);
    basicFighterValid(data, null);
    next();
}

const updateFighterValid = (req, res, next) => {
    const data = req.body;

    basicFighterValid(data, req.params.id);
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;