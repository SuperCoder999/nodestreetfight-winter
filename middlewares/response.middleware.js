const { HttpStatuses } = require('../config/http');

const responseMiddleware = (req, res, next) => {
    // Error handling is located at middlewares/errorHandler.middleware.js

    if (res.data && res.data.json) {
        res.status(res.data.status || HttpStatuses.OK).json(res.data.json);
    } else {
        res.status(HttpStatuses.OK).send();
    }

    next();
}

exports.responseMiddleware = responseMiddleware;