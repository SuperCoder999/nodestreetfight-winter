const { HttpStatuses } = require('../config/http');

exports.errorHandler = (err, req, res, next) => {
    res.status(err.status || HttpStatuses.BadRequest).json({
        error: true,
        message: err.message || 'Unknown error detected',
    });
};
