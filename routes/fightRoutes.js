const { Router } = require('express');
const FightController = require('../controllers/fight.controller');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router
    .get('/', FightController.list, responseMiddleware)
    .get('/:id', FightController.retreive, responseMiddleware)
    .post('/', createFightValid, FightController.create, responseMiddleware)
    .put('/:id', updateFightValid, FightController.partialUpdate, responseMiddleware)
    .delete('/:id', FightController.delete, responseMiddleware);

module.exports = router;