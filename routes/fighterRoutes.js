const { Router } = require('express');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const FighterController = require('../controllers/fighter.controller');

const router = Router();

router
    .get('/', FighterController.list, responseMiddleware)
    .get('/:id', FighterController.retreive, responseMiddleware)
    .post('/', createFighterValid, FighterController.create, responseMiddleware)
    .put('/:id', updateFighterValid, FighterController.partialUpdate, responseMiddleware)
    .delete('/:id', FighterController.delete, responseMiddleware);

module.exports = router;