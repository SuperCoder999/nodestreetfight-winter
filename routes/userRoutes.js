const { Router } = require('express');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const UserController = require('../controllers/user.controller');

const router = Router();

router
    .get('/', UserController.list, responseMiddleware)
    .get('/:id', UserController.retreive, responseMiddleware)
    .post('/', createUserValid, UserController.create, responseMiddleware)
    .put('/:id', updateUserValid, UserController.partialUpdate, responseMiddleware)
    .delete('/:id', UserController.delete, responseMiddleware);

module.exports = router;