const { Router } = require('express');
const AuthController = require('../controllers/auth.controller');
const { loginValid } = require('../middlewares/auth.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', loginValid, AuthController.login, responseMiddleware);

module.exports = router;