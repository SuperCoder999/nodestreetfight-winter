exports.fighter = {
    'id': '',
    'name': '',
    'health': 100,
    'power': 0,
    'defense': 1, // 1 to 10
};

exports.validationTemplate = {
    'name': '',
    'health': 0,
    'power': 0,
    'defense': 0,
};
