const { ValidationNumbers } = require('./validation');

exports.ErrorMessages = {
    InvalidEmail: 'Email is invalid',
    InvalidPhone: 'Phone number is invalid',
    InvalidPower: `Power must be ${ValidationNumbers.PowerMin} <= power <= ${ValidationNumbers.PowerMax}`,
    InvalidDefense: `Defense must be ${ValidationNumbers.DefenseMin} <= defense <= ${ValidationNumbers.DefenseMax}`,
    ShortPassword: `Password must be at least ${ValidationNumbers.PasswordMin} characters long`,
    Missing: ' is missing',
    Excess: ' is excess',
    UserNotFound: 'User not found',
    FighterNotFound: 'Fighter not found',
    FightNotFound: 'Fight not found',
    CredentialsInvalid: 'Email or password is invalid',
    EmailNonUnique: 'Email is non-unique',
    PhoneNonUnique: 'Phone number is non-unique',
    NameNonUnique: 'Name is non-unique',
};
