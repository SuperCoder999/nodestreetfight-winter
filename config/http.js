exports.HttpStatuses = {
    OK: 200,
    BadRequest: 400,
    NotAuthorized: 401,
    NotFound: 404,
};
