exports.ValidationRegexes = {
    Email: /^[a-zA-Z0-9.]+@gmail\.com$/,
    Phone: /^\+380[0-9]{9}$/,
};

exports.ValidationNumbers = {
    DefenseMin: 1,
    DefenseMax: 10,
    PowerMin: 1,
    PowerMax: 99,
    PasswordMin: 3,
};
