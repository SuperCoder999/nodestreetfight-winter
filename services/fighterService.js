const { ErrorMessages } = require('../config/error');
const { HttpStatuses } = require('../config/http');
const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    list() {
        const fighters = FighterRepository.getAll();
        return fighters;
    }

    retreive(search) {
        const item = FighterRepository.getOne(search);

        if (!item) {
            throw {
                status: HttpStatuses.NotFound,
                message: ErrorMessages.FighterNotFound,
            };
        }

        return item;
    }

    create(data) {
        const fighter = FighterRepository.create(data);
        return fighter;
    }

    partialUpdate(id, data)  {
        const check = FighterRepository.getOne({ id });

        if (!check) {
            throw {
                status: HttpStatuses.NotFound,
                message: ErrorMessages.FighterNotFound,
            };
        }

        const fighter = FighterRepository.update(id, data);
        return fighter;
    }

    delete(id) {
        const result = FighterRepository.delete(id);

        if (result.length === 0) {
            throw {
                status: HttpStatuses.NotFound,
                message: ErrorMessages.FighterNotFound,
            };
        }

        return result[0];
    }
}

module.exports = new FighterService();