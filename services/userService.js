const { HttpStatuses } = require('../config/http');
const { UserRepository } = require('../repositories/userRepository');
const { ErrorMessages } = require('../config/error');

class UserService {
    list() {
        const users = UserRepository.getAll();
        return users;
    }

    retreive(search) {
        const item = UserRepository.getOne(search);

        if (!item) {
            throw {
                status: HttpStatuses.NotFound,
                message: ErrorMessages.UserNotFound,
            };
        }

        return item;
    }

    create(data) {
        const user = UserRepository.create(data);
        return user;
    }

    partialUpdate(id, data)  {
        const check = UserRepository.getOne({ id });

        if (!check) {
            throw {
                status: HttpStatuses.NotFound,
                message: ErrorMessages.UserNotFound,
            };
        }

        const user = UserRepository.update(id, data);
        return user;
    }

    delete(id) {
        const result = UserRepository.delete(id);

        if (result.length === 0) {
            throw {
                status: HttpStatuses.NotFound,
                message: ErrorMessages.UserNotFound,
            };
        }

        return result[0];
    }
}

module.exports = new UserService();