const { ErrorMessages } = require('../config/error');
const { HttpStatuses } = require('../config/http');
const UserService = require('./userService');

class AuthService {
    login(userData) {
        try {
            const user = UserService.retreive(userData);
            return user;
        } catch {
            throw {
                status: HttpStatuses.NotAuthorized,
                message: ErrorMessages.CredentialsInvalid,
            };
        }
    }
}

module.exports = new AuthService();