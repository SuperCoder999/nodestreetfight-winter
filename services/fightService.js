const { FightRepository } = require('../repositories/fightRepository');
const { ErrorMessages } = require('../config/error');
const { HttpStatuses } = require('../config/http');

class FightersService {
    list() {
        const fights = FightRepository.getAll();
        return fights;
    }

    retreive(search) {
        const item = FightRepository.getOne(search);

        if (!item) {
            throw {
                status: HttpStatuses.NotFound,
                message: ErrorMessages.FightNotFound,
            };
        }

        return item;
    }

    create(data) {
        const fight = FightRepository.create(data);
        return fight;
    }

    partialUpdate(id, data)  {
        const check = FightRepository.getOne({ id });

        if (!check) {
            throw {
                status: HttpStatuses.NotFound,
                message: ErrorMessages.FightNotFound,
            };
        }

        const fight = FightRepository.update(id, data);
        return fight;
    }

    delete(id) {
        const result = FightRepository.delete(id);

        if (result.length === 0) {
            throw {
                status: HttpStatuses.NotFound,
                message: ErrorMessages.FightNotFound,
            };
        }

        return result[0];
    }
}

module.exports = new FightersService();