import { getCurrentTime } from "./timeHelper";
import { CRITICAL_TIMEOUT } from "../constants/controls";

export function transformFighterToPlayer(fighter) {
    return {
        ...fighter,
        lastCriticalAt: getCurrentTime() - CRITICAL_TIMEOUT,
    };
}

export function transformPlayerToFighter(player) {
    const { lastCriticalAt, ...fighter } = player;
    return fighter;
}
