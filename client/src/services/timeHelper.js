import { MILLISECONDS_IN_SECONDS } from "../constants/time";

export function getCurrentTime() {
    const now = performance.now();
    return now / MILLISECONDS_IN_SECONDS;
};
