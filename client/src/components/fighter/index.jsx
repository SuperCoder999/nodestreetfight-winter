import React, { useState } from 'react';
import { FormControl, InputLabel, makeStyles, Select } from '@material-ui/core';
import { MenuItem } from 'material-ui';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    hidden: {
        display: "none",
    },
}));

export default function Fighter({ fightersList, onFighterSelect, selectedFighter, hidden }) {
    const classes = useStyles();
    const [fighter, setFighter] = useState();
    const hiddenClass = hidden ? classes.hidden : undefined;

    const handleChange = (event) => {
        setFighter(event.target.value);
        onFighterSelect(event.target.value);
    };

    return (
        <div>
            <FormControl className={classes.formControl}>
                <InputLabel id="simple-select-label" className={hiddenClass}>Select Fighter</InputLabel>
                <Select
                    labelId="simple-select-label"
                    id="simple-select"
                    value={fighter}
                    onChange={handleChange}
                    className={hiddenClass}
                >
                    {fightersList.map((it, index) => {
                        return (
                            <MenuItem key={`${index}`} value={it}>{it.name}</MenuItem>
                        );
                    })}
                </Select>
                {selectedFighter
                    ? (
                        <div>
                            <div>{!hidden ? "Name: " : null}{selectedFighter.name}</div>
                            {!hidden
                                ? (
                                    <>
                                        <div>Power: {selectedFighter.power}</div>
                                        <div>Defense: {selectedFighter.defense}</div>
                                    </>
                                ) : null
                            }
                            <div>Health: {Math.max(selectedFighter.health.toFixed(1), 0)}</div>
                        </div>
                    ) : null
                }
            </FormControl>
        </div>)
}
