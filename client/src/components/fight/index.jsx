import React from 'react';

import { getFighters } from '../../services/domainRequest/fightersRequest';
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import { Button } from '@material-ui/core';
import { transformFighterToPlayer, transformPlayerToFighter } from '../../services/playerHelper';
import { CONTROLS, CRITICAL_TIMEOUT } from '../../constants/controls';
import { CHANCE_MIN, CHANCE_MAX  } from '../../constants/random';
import { getRandomBetween } from '../../services/randomHelper';
import { getCurrentTime } from '../../services/timeHelper';
import { createFight } from '../../services/domainRequest/fightRequest';

import './fight.css';

class Fight extends React.Component {
    state = {
        fighters: [],
        fighter1: null,
        fighter2: null,
        fightStarted: false,
    };

    async componentDidMount() {
        const fighters = await getFighters();

        if (fighters && !fighters.error) {
            this.setState({ fighters });
        }
    }

    getAttackPower = (fighter) => {
        const criticalHitChance = getRandomBetween(CHANCE_MIN, CHANCE_MAX);
        return fighter.power * criticalHitChance;
    }

    getBlockPower = (fighter) => {
        const dodgeChance = getRandomBetween(CHANCE_MIN, CHANCE_MAX);
        return fighter.defense * dodgeChance;
    }

    getDamage = (attacker, defender) => {
        const attack = this.getAttackPower(attacker);
        const defend = this.getBlockPower(defender);
        const result = attack - defend;
        const minTreshold = Math.max(result, 0);

        return minTreshold;
    }

    /* eslint-disable no-sequences */
    maybeAttack = ({
        attacker,
        defender,
        attackerKey,
        attackerBlockKey,
        defenderBlockKey,
        pressedKeys,
        log,
    }) => {
        const canAttack = pressedKeys.has(attackerKey);
        const attackerBlocking = pressedKeys.has(attackerBlockKey);
        const defenderBlocking = pressedKeys.has(defenderBlockKey);
        const blocking = attackerBlocking || defenderBlocking;

        if (canAttack && !blocking) {
            log.push(attacker.name + " attack");
            const damage = this.getDamage(attacker, defender);
            defender.health -= damage;
        }

        return attacker, defender, log;
    };

    maybeCriticalHit = ({
        attacker,
        defender,
        criticalCombo,
        pressedKeys,
        log,
    }) => {
        const time = getCurrentTime();

        if (time - attacker.lastCriticalAt < CRITICAL_TIMEOUT) {
            return attacker, defender, log;
        }

        let canHit = true;

        criticalCombo.forEach((key) => {
            const pressed = pressedKeys.has(key);

            if (!pressed) {
                canHit = false;
            }
        });

        if (canHit) {
            log.push(attacker.name + " critical");
            const damage = CHANCE_MAX * attacker.power;
            defender.health -= damage;
            attacker.lastCriticalAt = time;
        }

        return attacker, defender, log;
    }

    /* eslint-disable no-unused-expressions */
    performAttack = ({ player1, player2, pressedKeys, log }) => {
        player1, player2, log = this.maybeAttack({
            attacker: player1,
            defender: player2,
            attackerKey: CONTROLS.Player1Attack,
            attackerBlockKey: CONTROLS.Player1Block,
            defenderBlockKey: CONTROLS.Player2Block,
            pressedKeys,
            log,
        });

        player2, player1, log = this.maybeAttack({
            attacker: player2,
            defender: player1,
            attackerKey: CONTROLS.Player2Attack,
            attackerBlockKey: CONTROLS.Player2Block,
            defenderBlockKey: CONTROLS.Player1Block,
            pressedKeys,
            log,
        });
        console.log(log);

        return player1, player2, log;
    }

    performCritical = ({ player1, player2, pressedKeys, log }) => {
        player1, player2, log = this.maybeCriticalHit({
            attacker: player1,
            defender: player2,
            criticalCombo: CONTROLS.Player1Critical,
            pressedKeys,
            log,
        });

        player2, player1, log = this.maybeCriticalHit({
            attacker: player2,
            defender: player1,
            criticalCombo: CONTROLS.Player2Critical,
            pressedKeys,
            log,
        });

        return player1, player2, log;
    }

    onFightStart = () => {
        this.setState({ fightStarted: true });

        const { fighter1, fighter2 } = this.state;
        let player1 = transformFighterToPlayer(fighter1);
        let player2 = transformFighterToPlayer(fighter2);
        let log = [];
        const pressedKeys = new Set();

        document.onkeydown = (event) => {
            if (pressedKeys.has(event.code)) return;

            pressedKeys.add(event.code);
            const params = { player1, player2, pressedKeys, log };

            player1, player2, log = this.performAttack(params);
            player1, player2, log = this.performCritical(params);

            const newFighter1 = transformPlayerToFighter(player1);
            const newFighter2 = transformPlayerToFighter(player2);

            this.setState({
                fighter1: newFighter1,
                fighter2: newFighter2,
            });

            if (player1.health <= 0) {
                this.reportWinner({ winner: player2, player1, player2, log });
            } else if (player2.health <= 0) {
                this.reportWinner({ winner: player1, player1, player2, log });
            }
        };

        document.onkeyup = (event) => {
            pressedKeys.delete(event.code);
        };
    }
    /* eslint-enable no-unused-expressions */
    /* eslint-enable no-sequences */

    reportWinner = async ({ winner, player1, player2, log }) => {
        log.push(winner.name + " wins");

        await createFight({ fighter1: player1.id, fighter2: player2.id, log });
        alert(winner.name + " wins!");
        setTimeout(() => window.location.reload(), 400);
    }

    onCreate = (fighter) => {
        this.setState({ fighters: [...this.state.fighters, fighter] });
    }

    onFighter1Select = (fighter1) => {
        this.setState({ fighter1 });
    }

    onFighter2Select = (fighter2) => {
        this.setState({ fighter2 });
    }

    getFighter1List = () => {
        const { fighter2, fighters } = this.state;

        if (!fighter2) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter2.id);
    }

    getFighter2List = () => {
        const { fighter1, fighters } = this.state;

        if (!fighter1) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter1.id);
    }

    render() {
        const { fighter1, fighter2, fightStarted } = this.state;

        return (
            <div id="wrapper">
                <NewFighter onCreated={this.onCreate} />
                <div id="figh-wrapper">
                    <Fighter
                        selectedFighter={fighter1}
                        onFighterSelect={this.onFighter1Select}
                        fightersList={this.getFighter1List() || []}
                        hidden={fightStarted}
                    />
                    <div className="btn-wrapper" style={{ display: fightStarted ? "none" : "block" }}>
                        <Button onClick={this.onFightStart} variant="contained" color="primary">Start Fight</Button>
                    </div>
                    <Fighter
                        selectedFighter={fighter2}
                        onFighterSelect={this.onFighter2Select}
                        fightersList={this.getFighter2List() || []}
                        hidden={fightStarted}
                    />
                </div>
            </div>
        );
    }
}

export default Fight;