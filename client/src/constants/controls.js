export const CONTROLS = {
    Player1Attack: "KeyA",
    Player2Attack: "KeyJ",
    Player1Block: "KeyD",
    Player2Block: "KeyL",
    Player1Critical: ["KeyQ", "KeyW", "KeyE"],
    Player2Critical: ["KeyU", "KeyI", "KeyO"],
};

export const CRITICAL_TIMEOUT = 10;
